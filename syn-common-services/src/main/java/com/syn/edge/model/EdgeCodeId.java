package com.syn.edge.model;

import java.io.Serializable;

public class EdgeCodeId implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7141873439371453308L;
	
	public EdgeCodeId(){
		
	}
	
	
	public EdgeCodeId(String code,String type){
		this.code=code;
		this.type=type;
	}
	private String code;
	private String type;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
