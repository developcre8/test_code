package com.syn.edge.model;

// Generated Jul 21, 2014 4:04:15 PM by Hibernate Tools 4.0.0

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.syn.aco.common.json.SynRestBaseJSON;
import com.syn.aco.common.model.base.RestEntity;

/**
 * CorrespondanceNotification generated by hbm2java
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@SuppressWarnings("unused")
public class DataType extends SynRestBaseJSON<RestEntity> implements RestEntity   {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1563991482730716076L;
	@NotEmpty(message = "must be not empty")
	private String dataTypeId;
	@NotEmpty(message = "must be not empty")
	private String dataType;
	@JsonIgnore
	private Long createUserId;
	@JsonIgnore
	private Date createDateTime;
	@JsonIgnore
	private Long modifiedUserId;
	@JsonIgnore
	private Date modifiedDateTime;
	@JsonIgnore
	private Long dataTypeSysId;
	private List<DataTag> dataTag;
	
	
	public String getDataTypeId() {
		return dataTypeId;
	}
	public void setDataTypeId(String dataTypeId) {
		this.dataTypeId = dataTypeId;
	}
	
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public Long getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(Long i) {
		this.createUserId = i;
	}
	public Date getCreateDateTime() {
		return createDateTime;
	}
	public void setCreateDateTime(Date date) {
		this.createDateTime = date;
	}
	public Long getModifiedUserId() {
		return modifiedUserId;
	}
	public void setModifiedUserId(Long modifiedUserId) {
		this.modifiedUserId = modifiedUserId;
	}
	public Date getModifiedDateTime() {
		return modifiedDateTime;
	}
	public void setModifiedDateTime(Date date) {
		this.modifiedDateTime = date;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	
	public List<DataTag> getDataTag() {
		return dataTag;
	}
	public void setDataTag(List<DataTag> dataTag) {
		this.dataTag = dataTag;
	}
	public Long getDataTypeSysId() {
		return dataTypeSysId;
	}
	public void setDataTypeSysId(Long dataTypeSysId) {
		this.dataTypeSysId = dataTypeSysId;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
