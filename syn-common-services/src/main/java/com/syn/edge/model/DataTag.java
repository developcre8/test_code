package com.syn.edge.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.syn.aco.common.json.SynRestBaseJSON;
import com.syn.aco.common.model.base.RestEntity;


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)


public class DataTag extends SynRestBaseJSON<RestEntity> implements RestEntity  {
	
	
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDataTagId() {
		return dataTagId;
	}
	public void setDataTagId(String dataTagId) {
		this.dataTagId = dataTagId;
	}
	public String getDataTypeId() {
		return dataTypeId;
	}
	public void setDataTypeId(String dataTypeId) {
		this.dataTypeId = dataTypeId;
	}
	public String getDataTag() {
		return dataTag;
	}
	public void setDataTag(String dataTag) {
		this.dataTag = dataTag;
	}
	
	public String getRangeAllowed() {
		return rangeAllowed;
	}
	public void setRangeAllowed(String rangeAllowed) {
		this.rangeAllowed = rangeAllowed;
	}
	public String getNotRangeAllowed() {
		return notRangeAllowed;
	}
	public void setNotRangeAllowed(String notRangeAllowed) {
		this.notRangeAllowed = notRangeAllowed;
	}
	public Long getSubDataTagAllowed() {
		return subDataTagAllowed;
	}
	public void setSubDataTagAllowed(Long subDataTagAllowed) {
		this.subDataTagAllowed = subDataTagAllowed;
	}
	public String getRestCall() {
		return restCall;
	}
	public void setRestCall(String restCall) {
		this.restCall = restCall;
	}
	public Long getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(Long createUserId) {
		this.createUserId = createUserId;
	}
	public String getCreateDateTime() {
		return createDateTime;
	}
	public void setCreateDateTime(String createDateTime) {
		this.createDateTime = createDateTime;
	}
	public Long getModifiedUserId() {
		return modifiedUserId;
	}
	public void setModifiedUserId(Long modifiedUserId) {
		this.modifiedUserId = modifiedUserId;
	}
	public String getModifiedDateTime() {
		return modifiedDateTime;
	}
	public void setModifiedDateTime(String modifiedDateTime) {
		this.modifiedDateTime = modifiedDateTime;
	}
	private String dataTagId;
	private String dataTypeId;
	private String dataTag;
	private String rangeAllowed;
	private String notRangeAllowed;
	@JsonIgnore
	private Long subDataTagAllowed;
	private String restCall;
	@JsonIgnore
	private Long createUserId;
	@JsonIgnore
	private String createDateTime;
	@JsonIgnore
	private Long modifiedUserId;
	@JsonIgnore
	private String modifiedDateTime;
	
	private String dataTagSysId;
	private DataType dataType;
	
	public String getDataTagSysId() {
		return dataTagSysId;
	}
	public void setDataTagSysId(String dataTagSysId) {
		this.dataTagSysId = dataTagSysId;
	}
	public DataType getDataType() {
		return dataType;
	}
	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}
	

}
