package com.syn.edge.model;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.syn.aco.common.json.SynRestBaseJSON;
import com.syn.aco.common.model.base.RestEntity;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Schedules extends SynRestBaseJSON<RestEntity> implements RestEntity {
	
	private String id;
	private String scheduleType;
	private String scheduleName;
	private String vendorId;
	private String scheduleDesc;
	private String scheduleEffectiveDate;
	private String scheduleStatus;
	private String scheduleNextDate;
	private String scheduleStartTime;
	private String scheduleEndTime;
	private String scheduleOption1;
	private String scheduleOption2;
	private String scheduleOption3;
	private String scheduleNotification;
	private String notificationId;
	private String dataType;
	private String scheduleRunDate;
	private Integer createUserId;
	private Date createDateTime;
	private Integer updateUserId;
	private Date updateDateTime;
	private EdgeCode edgeCodeScheduleType;
	private EdgeCode edgeCodeScheduleStatus;	
	
	public EdgeCode getEdgeCodeScheduleType() {
		return edgeCodeScheduleType;
	}
	public void setEdgeCodeScheduleType(EdgeCode edgeCodeScheduleType) {
		this.edgeCodeScheduleType = edgeCodeScheduleType;
	}
	public EdgeCode getEdgeCodeScheduleStatus() {
		return edgeCodeScheduleStatus;
	}
	public void setEdgeCodeScheduleStatus(EdgeCode edgeCodeScheduleStatus) {
		this.edgeCodeScheduleStatus = edgeCodeScheduleStatus;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getScheduleType() {
		return scheduleType;
	}
	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}
	public String getScheduleName() {
		return scheduleName;
	}
	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getScheduleDesc() {
		return scheduleDesc;
	}
	public void setScheduleDesc(String scheduleDesc) {
		this.scheduleDesc = scheduleDesc;
	}
	public String getScheduleEffectiveDate() {
		return scheduleEffectiveDate;
	}
	public void setScheduleEffectiveDate(String scheduleEffectiveDate) {
		this.scheduleEffectiveDate = scheduleEffectiveDate;
	}
	public String getScheduleStatus() {
		return scheduleStatus;
	}
	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}
	public String getScheduleNextDate() {
		return scheduleNextDate;
	}
	public void setScheduleNextDate(String scheduleNextDate) {
		this.scheduleNextDate = scheduleNextDate;
	}
	public String getScheduleStartTime() {
		return scheduleStartTime;
	}
	public void setScheduleStartTime(String scheduleStartTime) {
		this.scheduleStartTime = scheduleStartTime;
	}
	public String getScheduleEndTime() {
		return scheduleEndTime;
	}
	public void setScheduleEndTime(String scheduleEndTime) {
		this.scheduleEndTime = scheduleEndTime;
	}
	public String getScheduleOption1() {
		return scheduleOption1;
	}
	public void setScheduleOption1(String scheduleOption1) {
		this.scheduleOption1 = scheduleOption1;
	}
	public String getScheduleOption2() {
		return scheduleOption2;
	}
	public void setScheduleOption2(String scheduleOption2) {
		this.scheduleOption2 = scheduleOption2;
	}
	public String getScheduleOption3() {
		return scheduleOption3;
	}
	public void setScheduleOption3(String scheduleOption3) {
		this.scheduleOption3 = scheduleOption3;
	}
	public String getScheduleNotification() {
		return scheduleNotification;
	}
	public void setScheduleNotification(String scheduleNotification) {
		this.scheduleNotification = scheduleNotification;
	}
	public String getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getScheduleRunDate() {
		return scheduleRunDate;
	}
	public void setScheduleRunDate(String scheduleRunDate) {
		this.scheduleRunDate = scheduleRunDate;
	}
	public Integer getCreateUserId() {
		return createUserId;
	}
	public void setCreateUserId(Integer createUserId) {
		this.createUserId = createUserId;
	}
	public Date getCreateDateTime() {
		return createDateTime;
	}
	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}
	public Integer getUpdateUserId() {
		return updateUserId;
	}
	public void setUpdateUserId(Integer updateUserId) {
		this.updateUserId = updateUserId;
	}
	public Date getUpdateDateTime() {
		return updateDateTime;
	}
	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}
	
	

}
