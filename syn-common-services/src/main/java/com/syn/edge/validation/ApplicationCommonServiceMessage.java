package com.syn.edge.validation;

public interface ApplicationCommonServiceMessage {
	
	String ADD_FAILED="Add failed, Please try again.";
	String WORKFLOW_NAME_EXIST="Workflow name alredy exist, please enter other.";
	String RULE_NAME_EXIST = "Rule Name alredy exist, please enter other.";
	String OUTBD_VENDOR_ID_and_DATA_TYPE_EXIST = "OutbdVendorId and DataType already exist, please enter another pair";
	String VENDOR_ID_and_DATA_TYPE_EXIST = "VendorId and DataType already exist, please enter another pair";
	String INBD_VENDOR_ID_and_RULESET_ID_EXIST = "InbdVendorId and RulesetId already exist, please enter another pair";
	String RULESET_ID_and_RULE_ID_EXIST = "RuleSetId and RuleId already exist, please enter another pair";
	String DATATYPE_ID_EXIST="LineOfBusiness Id Already Exist Please Enter the Another";
	String PAYER_ID_EXIST="Payer Id Already Exist Please Enter Other";
	String  MEMBER_ID_EXIST="Member Id already Exist,please try  another one.";
	String CODE_OR_TYPE_EXIST = "Code and Type already exist, please enter another. ";
}
