package com.syn.edge.ws;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.syn.aco.common.utils.ObjectJSReturn;
import com.syn.aco.common.validation.InvalidRequestException;
import com.syn.aco.common.ws.RestBaseEndPoint;
import com.syn.aco.common.ws.SynRestBaseEndPointI;
import com.syn.aco.common.ws.SynRestEndPointHandler;
import com.syn.aco.common.ws.SynRestSuccess;
import com.syn.edge.model.Schedules;
import com.syn.edge.services.api.SchedulesService;
import com.syn.edge.validation.ApplicationCommonServiceMessage;

@Controller
@RequestMapping("/schedules")
public class SchedulesEndPoint  extends SynRestEndPointHandler implements SynRestBaseEndPointI<Schedules,String> {
	
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private SchedulesService schedulesService;
	
	@Override
	 @RequestMapping(method = RequestMethod.GET, produces = "application/json")
	@RequiresPermissions("schedules:view") 
	public @ResponseBody SynRestSuccess findAll() {
		  List<Schedules> allList = schedulesService.findAll();
			 
		    return restSuccessResponse(allList);
	}

	@Override
	 @RequestMapping(value = "{id}", method = RequestMethod.GET,produces = "application/json")
	@RequiresPermissions("schedules:view")   
	public
	  @ResponseBody SynRestSuccess get(@PathVariable String id) {
		Schedules dataObject = schedulesService.get(id);
		return restSuccessResponse(dataObject);
	}
	@Override
	 @RequestMapping(method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
	  @ResponseStatus(HttpStatus.CREATED)
	@RequiresPermissions("schedules:create")  
	public
	  @ResponseBody SynRestSuccess create(@Valid @RequestBody Schedules core , BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(ApplicationCommonServiceMessage.ADD_FAILED, bindingResult);
        }
		Schedules dataObject = schedulesService.create(core);
		
		return restSuccessResponse(dataObject);
	}

	@Override
	 @RequestMapping(value = "{id}", method = RequestMethod.PUT,consumes = "application/json", produces = "application/json")
	@RequiresPermissions("schedules:edit")   
	public
	  @ResponseBody SynRestSuccess update(@RequestBody Schedules data, @PathVariable String id, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(ApplicationCommonServiceMessage.ADD_FAILED, bindingResult);
        }
		Schedules dataObject = schedulesService.update(data,id);
		return restSuccessResponse(dataObject);
	}
	@Override
	public SynRestSuccess delete(String id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@RequestMapping(value = "/find",method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
	public Map<String, Object> find(@RequestBody Schedules criteria) {
		
		return ObjectJSReturn.mapOK(schedulesService.find(criteria));
	}

	@Override
	@RequestMapping(value = "/findByLimit/{start}/{limit}", method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
	@RequiresPermissions("schedules:view") 
	public @ResponseBody Map<String,Object> findByLimit(@RequestBody Schedules criteria, @PathVariable int start, @PathVariable int limit) {
		 List<Schedules> totalList  = schedulesService.findByLimit(criteria, start, limit);
		 Integer totalCount = schedulesService.find(criteria).size();
		 return ObjectJSReturn.mapOK(totalList, totalCount);
	}
	@Override
	public Map<String, Object> getByLimit(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

}
