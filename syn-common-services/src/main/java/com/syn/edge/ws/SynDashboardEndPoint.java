package com.syn.edge.ws;


import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.syn.aco.common.ws.SynRestBaseEndPoint;
import com.syn.aco.common.ws.SynRestEndPointHandler;
import com.syn.aco.common.ws.SynRestSuccess;
import com.syn.edge.model.SynDashboard;
import com.syn.edge.services.api.SynDashboardService;



@RestController
@RequestMapping("/dashboard")
public class SynDashboardEndPoint extends SynRestEndPointHandler implements SynRestBaseEndPoint<SynDashboard,Long>{
	
	
	
	@Autowired
	private SynDashboardService synDashboardService;
	
	@RequestMapping(value = "/dashBoardData",method = RequestMethod.GET)
	 public SynRestSuccess synDashBoardData() {
		System.out.println("Hello");
		List<SynDashboard> allList = synDashboardService.getDashboradData();
			
		    return restSuccessResponse(allList);
	}
	
	@Override
	 @RequestMapping(value = "/getALL",method = RequestMethod.GET)
	 public SynRestSuccess findAll() {
		System.out.println("Hello");
		  List<SynDashboard> allList = synDashboardService.findAll();
			
		    return restSuccessResponse(allList);
	}

	@Override
	 @RequestMapping(value = "{id}", method = RequestMethod.GET,produces = "application/json")
	  public
	  @ResponseBody SynRestSuccess get(@PathVariable Long id) {
		//SynEdgeOutbdBatchDetail dataObject = synedgeOutbdBatchDetail.get(id);
		return restSuccessResponse("");
	}

	@Override
	 @RequestMapping(method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
	  @ResponseStatus(HttpStatus.CREATED)
	  public
	  @ResponseBody SynRestSuccess create(@Valid @RequestBody SynDashboard core , BindingResult bindingResult) {
		/*if (bindingResult.hasErrors()) {
            throw new InvalidRequestException("Add failed, Please try again ", bindingResult);
        }*/

		//SynEdgeOutbdBatchDetail dataObject = synedgeOutbdBatchDetail.create(core);
		synDashboardService.create(core);
		return restSuccessResponse("");
	}

	@Override
	 @RequestMapping(value = "{id}", method = RequestMethod.PUT,consumes = "application/json", produces = "application/json")
	  public
	  @ResponseBody SynRestSuccess update(@RequestBody SynDashboard data, @PathVariable Long id) {
		//SynEdgeOutbdBatchDetail dataObject = synedgeOutbdBatchDetail.update(data, Long.valueOf(id));
		return restSuccessResponse("");
	}


	@Override
	public SynRestSuccess delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public @ResponseBody Map<String, Object> find(SynDashboard criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> findByLimit(SynDashboard criteria, int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> getByLimit(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}



	
	
}
