package com.syn.edge.ws;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.syn.aco.common.utils.ObjectJSReturn;
import com.syn.aco.common.validation.InvalidRequestException;
import com.syn.aco.common.ws.SynRestBaseEndPoint;
import com.syn.aco.common.ws.SynRestEndPointHandler;
import com.syn.aco.common.ws.SynRestSuccess;
import com.syn.edge.model.EdgeCode;
import com.syn.edge.services.api.EdgeCodesService;
import com.syn.edge.validation.ApplicationCommonServiceMessage;

@Controller
@RequestMapping("/edgeCodes")
public class  EdgeCodesEndPoint extends SynRestEndPointHandler implements SynRestBaseEndPoint<EdgeCode,String>{
	
	@Autowired
	private EdgeCodesService edgeCodesService;
	
	
	@Override
	 @RequestMapping(method = RequestMethod.GET, produces = "application/json")
	 public @ResponseBody SynRestSuccess findAll() {
		  List<EdgeCode> allList = edgeCodesService.findAll();
			 
		    return restSuccessResponse(allList);
	}

	@Override
	 @RequestMapping(value = "{id}", method = RequestMethod.GET,produces = "application/json")
	  public
	  @ResponseBody SynRestSuccess get(@PathVariable String id) {
		EdgeCode dataObject = edgeCodesService.get(id);
		return restSuccessResponse(dataObject);
	}

	@Override
	 @RequestMapping(method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
	  @ResponseStatus(HttpStatus.CREATED)
	  public
	  @ResponseBody SynRestSuccess create(@Valid @RequestBody EdgeCode core , BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(ApplicationCommonServiceMessage.ADD_FAILED, bindingResult);
        }

		if (edgeCodesService.isDuplicateCreate(core)) {
            throw new InvalidRequestException(ApplicationCommonServiceMessage.CODE_OR_TYPE_EXIST, bindingResult);
        }
		EdgeCode dataObject = edgeCodesService.create(core);
		
		return restSuccessResponse(dataObject);
	}

	@Override
	 @RequestMapping(value = "{id}", method = RequestMethod.PUT,consumes = "application/json", produces = "application/json")
	  public
	  @ResponseBody SynRestSuccess update(@RequestBody EdgeCode data, @PathVariable String id) {
		EdgeCode dataObject = edgeCodesService.update(data, id);
		return restSuccessResponse(dataObject);
	}


	@Override
	public SynRestSuccess delete(String id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Map<String, Object> find(EdgeCode criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@RequestMapping(value = "/findByLimit/{start}/{limit}", method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
 	@RequiresPermissions("user:view")
	public @ResponseBody Map<String,Object> findByLimit(@RequestBody EdgeCode criteria, @PathVariable int start, @PathVariable int limit) {
		 List<EdgeCode> totalList  = edgeCodesService.findByLimit(criteria, start, limit);
		 Integer totalCount = edgeCodesService.count(criteria);
		 return ObjectJSReturn.mapOK(totalList, totalCount);
	}


	@Override
	public Map<String, Object> getByLimit(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	 @RequestMapping(value = "/getBy/{code}/{type}", method = RequestMethod.GET,produces = "application/json")
	  public
	  @ResponseBody SynRestSuccess getByCodeOrName(@PathVariable String code,@PathVariable String type) {
		 EdgeCode edgeCode = new EdgeCode();
		 edgeCode.setCode(code);
		 edgeCode.setType(type);
		List<EdgeCode> dataObject = edgeCodesService.getEdgeCode(edgeCode);
		return restSuccessResponse(dataObject);
	}
	 
	 @RequestMapping(value = "/getByCode/{code}/", method = RequestMethod.GET,produces = "application/json")
	  public
	  @ResponseBody SynRestSuccess getByCode(@PathVariable String code) {
		 
		List<EdgeCode> dataObject = edgeCodesService.getByCode(code);
		return restSuccessResponse(dataObject);
	}
	 
	 
	 @RequestMapping(value = "/getByType/{type}", method = RequestMethod.GET,produces = "application/json")
	  public
	  @ResponseBody SynRestSuccess getByCodeOrName(@PathVariable String type) {
		 List<EdgeCode> dataObject = edgeCodesService.getByType(type);
		return restSuccessResponse(dataObject);
	}
	 
	 
	 @RequestMapping(value = "/likeBy/{name}", method = RequestMethod.GET,produces = "application/json")
	  public
	  @ResponseBody SynRestSuccess likeByName(@PathVariable String name) {
		 List<EdgeCode> dataObject = edgeCodesService.likeByName(name);
		return restSuccessResponse(dataObject);
	}
	 
	
	
}
