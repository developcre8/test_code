package com.syn.edge.ws;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.syn.aco.common.utils.ObjectJSReturn;
import com.syn.aco.common.validation.InvalidRequestException;
import com.syn.aco.common.ws.SynRestBaseEndPointI;
import com.syn.aco.common.ws.SynRestEndPointHandler;
import com.syn.aco.common.ws.SynRestSuccess;
import com.syn.edge.model.DataType;
import com.syn.edge.services.api.DataTypeService;
import com.syn.edge.validation.ApplicationCommonServiceMessage;

@Controller
@RequestMapping("/dataType")
public class DataTypeEndPoint extends SynRestEndPointHandler implements SynRestBaseEndPointI<DataType,String> {
	
	
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private DataTypeService dataTypeService;
	@Override
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	@RequiresPermissions("dataType:view")
	public @ResponseBody SynRestSuccess findAll() {
		 List<DataType> allList = dataTypeService.findAll();
		   return restSuccessResponse(allList);
	}

	@Override
	@RequestMapping(value = "{id}", method = RequestMethod.GET,produces = "application/json")
	@RequiresPermissions("dataType:view")
	public @ResponseBody SynRestSuccess get(@PathVariable String id) {
		DataType dataObject = dataTypeService.get(id);
		return restSuccessResponse(dataObject);
		
	}

	@Override
	@RequestMapping(method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	 @RequiresPermissions("dataType:create")
	public
	@ResponseBody SynRestSuccess create(@Valid @RequestBody DataType entity,
			BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
            throw new InvalidRequestException(ApplicationCommonServiceMessage.ADD_FAILED, bindingResult);
        }
		if (dataTypeService.isDuplicateCreate(entity)) {
            throw new InvalidRequestException(ApplicationCommonServiceMessage.DATATYPE_ID_EXIST, bindingResult);
        }
		DataType dataObject = dataTypeService.create(entity);
		return restSuccessResponse(dataObject);
	}

	@Override
	@RequestMapping(value = "{id}", method = RequestMethod.PUT,consumes = "application/json", produces = "application/json")
	@RequiresPermissions("dataType:edit")
	  public
	  @ResponseBody SynRestSuccess update(@Valid  @RequestBody DataType data, @PathVariable String id, BindingResult bindingResult) {
			
			if (bindingResult.hasErrors()) {
	           throw new InvalidRequestException(ApplicationCommonServiceMessage.ADD_FAILED, bindingResult);
	       }
			if (dataTypeService.isDuplicateUpdate(data,id)) {
	            throw new InvalidRequestException(ApplicationCommonServiceMessage.DATATYPE_ID_EXIST, bindingResult);
	        }
		DataType dataObject = dataTypeService.update(data,id);
		return restSuccessResponse(dataObject);
	}

	@Override
	public SynRestSuccess delete(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public Map<String, Object> find(DataType criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	//@RequestMapping(value = "/findByLimit/{start}/{limit}", method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
	public @ResponseBody Map<String, Object> findByLimit(@RequestBody DataType criteria,@PathVariable	int start,@PathVariable int limit) {
		List<DataType> totalList  = dataTypeService.findByLimit(criteria, start, limit);
		 Integer totalCount = dataTypeService.count();
		 return ObjectJSReturn.mapOK(totalList, totalCount);
	}

	@Override
	//@RequestMapping(value = "/getByLimit/{start}/{limit}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody Map<String,Object> getByLimit(@PathVariable int start, @PathVariable int limit) {
		List<DataType> totalList  = dataTypeService.getByLimit(start, limit);
		Integer totalCount = dataTypeService.count();
		 return ObjectJSReturn.mapOK(totalList, totalCount);
	}


}