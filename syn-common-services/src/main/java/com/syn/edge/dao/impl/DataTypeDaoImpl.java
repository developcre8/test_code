package com.syn.edge.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.syn.aco.common.dao.HibernateBaseDao;
import com.syn.edge.dao.api.DataTypeDao;
//import com.syn.edge.model.ClinicalParameters;
import com.syn.edge.model.DataType;

@Repository("dataTypeDao")
public class DataTypeDaoImpl implements DataTypeDao {
	
	@Autowired
	private HibernateBaseDao hibernateBaseDao;

	@Override
	public DataType findById(Serializable id) {
		Map where = new HashMap();
		where.put("dataTypeId", id);
		return (DataType)hibernateBaseDao.getAllListWithWhereOrdrByAsc("dataType_Select", where,"id").get(0);
		//return ( DataType)hibernateBaseDao.getObjectById("dataType_Select", id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataType> findAll() {
		Map where = new HashMap();
		return (List<DataType>)hibernateBaseDao.getAllListWithWhereOrdrByAsc("dataType_Select", where,"id");
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataType> find(DataType searchCriteria) {
		Map where = new HashMap();
		return (List< DataType>)hibernateBaseDao.getAllListWithWhereOrdrByDsc("dataType_Select", where,"createDateTime");
	}

	/*@Override
	public boolean isDuplicate(DataType entity) {
		Boolean b = false ;
		Map where = new HashMap();
		@SuppressWarnings("unchecked")
		List<DataType> dataType = (List<DataType>) hibernateBaseDao.getAllListWithWhereOrdrByAsc("dataType", where,"id");
		for(int i = 0; i<dataType.size();i++){
			DataType dataTypes=(DataType)dataType.get(i);
			if(dataTypes.getDataTypeId().equalsIgnoreCase(entity.getDataTypeId()))
			{
				b=true;
			}	
			
		}
		return b;
	}
	*/

	@Override
	public DataType insert(DataType entity) {
		String id = (String)hibernateBaseDao.saveData("dataType", entity);
		return findById(id);
	}

	@Override
	public DataType update(DataType entity) {
		@SuppressWarnings("unused")
		Boolean isUpdate = hibernateBaseDao.updateData("dataType", entity);		
		return findById(entity.getDataTypeId());
	}

	
	/*@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public int count() {
		Map<String, Object> where = new HashMap<String, Object>();
		Integer count = 0;
		try {
			count = hibernateBaseDao.getListSizeWithWhereClause("dataType", where);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataType> findByLimit(DataType searchCriteria,
			int start, int limit) {
		Map<String, Object> where = new HashMap<String, Object>();	
		  return (List<DataType>)hibernateBaseDao.getAllListWithWhereOrdrByDscLimit("dataType", where, "createDateTime", start, limit);
		
			
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataType> getByLimit(int start, int limit) {
		Map<String, Object> where = new HashMap<String, Object>();	
		  return (List<DataType>)hibernateBaseDao.getAllListWithWhereOrdrByDscLimit("dataType", where, "createDateTime", start, limit);
	}

	@Override
	public int count(DataType criteria) {
		Map<String, Object> where = new HashMap<String, Object>();
		Integer count = 0;
		try {
			count = hibernateBaseDao.getListSizeWithWhereClause("dataType", where);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	@Override
	public boolean isDuplicateInsert(DataType entity) {
		Map where = new HashMap();
		where.put("dataTypeId", entity.getDataTypeId());
		return hibernateBaseDao.isExistSave("dataType_duplicate", where);
	}

	@Override
	public void delete(Serializable id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDuplicateUpdate(DataType entity) {
		Map where = new HashMap();
		where.put("dataTypeId", entity.getDataTypeId());
		return hibernateBaseDao.isExistUpdate("dataType_duplicate", "id", entity.getDataTypeId(), where);
	}

}
