package com.syn.edge.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.syn.aco.common.dao.HibernateBaseDao;
import com.syn.edge.dao.api.DataTagDao;
import com.syn.edge.model.DataTag;
//import com.syn.edge.model.ParameterDetail;

@Repository("dataTagDao")
public class DataTagDaoImpl implements DataTagDao {

	@Autowired
	private HibernateBaseDao hibernateBaseDao;

	@Override
	public DataTag findById(Serializable id) {
		return ( DataTag)hibernateBaseDao.getObjectById("dataTag", id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataTag> findAll() {
		
		Map where = new HashMap();
		return (List<DataTag>)hibernateBaseDao.getAllListWithWhere("dataTag_select", where);	
			
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataTag> find(DataTag searchCriteria) {
		Map where = new HashMap();
		return (List< DataTag>)hibernateBaseDao.getAllListWithWhereOrdrByDsc("dataTag_select", where,"id");
	}

	@Override
	public boolean isDuplicate(DataTag entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DataTag insert(DataTag entity) {
		String id = (String)hibernateBaseDao.saveData("dataTag", entity);
		return findById(id);
	}

	@Override
	public DataTag update(DataTag entity) {
		Boolean isUpdate = hibernateBaseDao.updateData("dataTag", entity);		
		return findById(entity.getId());
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int count() {
		Map<String, Object> where = new HashMap<String, Object>();
		Integer count = 0;
		try {
			count = hibernateBaseDao.getListSizeWithWhereClause("dataTag", where);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataTag> findByLimit(DataTag searchCriteria,
			int start, int limit) {
		
//		Map<String, Object> where = new HashMap<String, Object>();	
//		return (List<DataTag>)hibernateBaseDao.getAllListWithWhereOrdrByDscLimit("dataTag", where, "clinicalParameterID", start, limit);
			return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DataTag> getByLimit(int start, int limit) {
		
//		Map<String, Object> where = new HashMap<String, Object>(); 
//		return (List<DataTag>)hibernateBaseDao.getAllListWithWhereOrdrByDscLimit("dataTag", where, "clinicalParameterID", start, limit);
		return null;
	}

}
