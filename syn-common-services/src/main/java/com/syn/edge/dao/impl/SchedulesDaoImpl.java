package com.syn.edge.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.syn.aco.common.dao.HibernateBaseDao;
import com.syn.edge.dao.api.SchedulesDao;
import com.syn.edge.model.Schedules;

@Repository("schedulesDao")
public class SchedulesDaoImpl implements SchedulesDao {

	@Autowired
	private HibernateBaseDao hibernateBaseDao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Schedules findById(Serializable id) {
		return (Schedules)hibernateBaseDao.getObjectById("schedules", id);
	}

	@Override
	public List<Schedules> findAll() {
		Map where = new HashMap();
		return (List<Schedules>)hibernateBaseDao.getAllListWithWhere("schedules", where);
	}
	@Transactional(readOnly = true)
	@Override
	public List<Schedules> find(Schedules searchCriteria) {
		Session session = sessionFactory.getCurrentSession();
		List<Schedules> schedules = null;
		try {
			Criteria queryCriteria = session.createCriteria("schedules");
			
			
			if(searchCriteria != null){
				if(searchCriteria.getScheduleName() != null && searchCriteria.getScheduleName() != ""){
					queryCriteria.add(Restrictions.eq("scheduleName", searchCriteria.getScheduleName()));
				}
			
			}
			
			schedules = queryCriteria.list();
		
		} catch (Exception e) {
		e.printStackTrace();
		}finally{
			//session.close();
		}
		
		return schedules;
	}

	@Override
	public Schedules insert(Schedules entity) {
		String id = (String)hibernateBaseDao.saveData("schedules_save", entity);
		return findById(id);
	}

	@Override
	public Schedules update(Schedules entity) {
		Boolean isUpdate = hibernateBaseDao.updateData("schedules_save", entity);
		return findById(entity.getId());
	}

	@Override
	public int count() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	@Override
	public List<Schedules> findByLimit(Schedules searchCriteria, int start, int limit) {
		Session session = sessionFactory.getCurrentSession();
		List<Schedules> schedules = null;
		try {
			Criteria queryCriteria = session.createCriteria("schedules");
			
			
			if(searchCriteria != null){
				if(searchCriteria.getScheduleName() != null && searchCriteria.getScheduleName() != ""){
					queryCriteria.add(Restrictions.eq("scheduleName", searchCriteria.getScheduleName()));
				}
				
				/*if(searchCriteria.getCreateDateTime() != null){
					queryCriteria.add(Restrictions.eq("createDateTime", searchCriteria.getCreateDateTime()));
					
				}*/
			
			}
			queryCriteria.setFirstResult(start);
			queryCriteria.setMaxResults(limit);
			schedules = queryCriteria.list();
		
		} catch (Exception e) {
		e.printStackTrace();
		}finally{
			//session.close();
		}
		
		return schedules;
	}

	@Override
	public List<Schedules> getByLimit(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isDuplicateInsert(Schedules entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void delete(Serializable id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDuplicateUpdate(Schedules entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int count(Schedules criteria) {
		// TODO Auto-generated method stub
		return 0;
	}

}
