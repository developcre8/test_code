package com.syn.edge.dao.api;

import java.util.List;

import com.syn.aco.common.dao.DaoI;
import com.syn.edge.model.EdgeCode;

public interface EdgeCodesDao extends DaoI<EdgeCode> {

	public List<EdgeCode> getByCode(String code);

	public List<EdgeCode> getByType(String type);

	public List<EdgeCode> likeByName(String name);

	public List<EdgeCode> getEdgeCode(EdgeCode edgeCode);

}
