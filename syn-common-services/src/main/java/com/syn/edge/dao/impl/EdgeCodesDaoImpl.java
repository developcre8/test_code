package com.syn.edge.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.syn.aco.common.dao.HibernateBaseDao;
import com.syn.edge.dao.api.EdgeCodesDao;
import com.syn.edge.model.EdgeCode;
import com.syn.edge.model.EdgeCodeId;


@Repository("edgeCodesDao")
public class EdgeCodesDaoImpl implements EdgeCodesDao {

	@Autowired
	private HibernateBaseDao hibernateBaseDao;
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public EdgeCode findById(Serializable id) {
		
		return (EdgeCode)hibernateBaseDao.getObjectById("edge_codes", id);
	}
	
	public EdgeCode findByComositeKeyId(EdgeCodeId id) {
		
		return (EdgeCode)hibernateBaseDao.getObjectById("edge_codes", id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EdgeCode> findAll() {
		Map where = new HashMap();
		return (List<EdgeCode>)hibernateBaseDao.getAllListWithWhere("edge_codes", where);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EdgeCode> find(EdgeCode searchCriteria) {
		Map where = new HashMap();
		return (List<EdgeCode>)hibernateBaseDao.getAllListWithWhere("edge_codes", where);
	}


	@Override
	public EdgeCode insert(EdgeCode entity) {
		EdgeCodeId id = (EdgeCodeId)hibernateBaseDao.saveData("edge_codes_save", entity);
		return findByComositeKeyId(id);
	}

	@Override
	public EdgeCode update(EdgeCode entity) {
		Boolean isUpdate = hibernateBaseDao.updateData("edge_codes_update", entity);
		
		return findByComositeKeyId(entity.getId());
	}

	

	@Override
	public int count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<EdgeCode> findByLimit(EdgeCode criteria, int start,	int limit) {
		Map eqMap = new HashMap();
		Map likeMap = new HashMap();
		likeMap.put("name", criteria.getCodeName());
		return hibernateBaseDao.getListWithWhereClauseDescLimit("edge_codes", eqMap, likeMap, null, null, "createdDate", start, limit);
	}
	
	@Override
	public int count(EdgeCode criteria) {
		Map eqMap = new HashMap();
		Map likeMap = new HashMap();
		likeMap.put("name", criteria.getCodeName());
		return hibernateBaseDao.getListWithWhereClauseSize("edge_codes", eqMap, likeMap, null, null, "createdDate");
	}

	@Override
	public List<EdgeCode> getByLimit(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EdgeCode> likeByName(String name){
		Session session = sessionFactory.getCurrentSession();
		List<EdgeCode> list=new ArrayList<EdgeCode>();
		Criteria query;
					try {
				
				query = session.createCriteria("edge_codes_select");
				if(name!=null && !name.equals("")){
				query.add(Restrictions.ilike("codeName", name, MatchMode.ANYWHERE));	
				}
				query.addOrder(Order.asc("codeName"));
				list = query.list();
				
			return list;	
			} catch (HibernateException e) {
				throw e;
			}
			
		}
	
	@Override
	public List<EdgeCode> getByType(String type)
	{
		Session session = sessionFactory.getCurrentSession();
		
		List<EdgeCode> list=new ArrayList<EdgeCode>();
		Criteria query;
		if(type!=null)
		{
			try {
				
				query = session.createCriteria("edge_codes_select");
				query.add(Restrictions.ilike("type", type, MatchMode.ANYWHERE));	
				query.addOrder(Order.asc("name"));
				list = query.list();
				
			return list;	
			} catch (HibernateException e) {
				throw e;
			} 
			
			
		}
	
	return list;	
	}
	
	@Override
	public List<EdgeCode> getByCode(String code)
	{
		Session session = sessionFactory.getCurrentSession();
		
		List<EdgeCode> list=new ArrayList<EdgeCode>();
		Criteria query;
		if(code!=null)
		{

			try {
				
				query = session.createCriteria("edge_codes_select");
				query.add(Restrictions.eq("code", code));		
				query.addOrder(Order.asc("name"));
				list = query.list();
				
			return list;	
			} catch (HibernateException e) {
				throw e;
			} 
			
			
		}
	
	return list;	
	}
	

	@Override
	public List<EdgeCode> getEdgeCode(EdgeCode edgeCode) throws HibernateException {
		Session session = sessionFactory.getCurrentSession();
		
		List<EdgeCode> list=null;
		Criteria query;
		try {
			query = session.createCriteria("edge_codes_select");
			if(edgeCode != null && edgeCode.getCode() != null && !edgeCode.getCode().equals("") && !edgeCode.getCode().equals("null")){
				query.add(Restrictions.eq("code", edgeCode.getCode()));	
			}
			if(edgeCode != null && edgeCode.getType() != null && !edgeCode.getType().equals("") && !edgeCode.getType().equals("null")){
				query.add(Restrictions.eq("type", edgeCode.getType()));	
			}
			query.addOrder(Order.asc("name"));
			list = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public boolean isDuplicateInsert(EdgeCode entity) {
		Map where = new HashMap();
		where.put("code", entity.getCode());
		where.put("type", entity.getType());
		return hibernateBaseDao.isExistSave("edge_codes_duplicate", where);
	}

	@Override
	public void delete(Serializable id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDuplicateUpdate(EdgeCode entity) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
