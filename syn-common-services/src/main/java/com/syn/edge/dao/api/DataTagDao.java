package com.syn.edge.dao.api;

import java.util.List;

import com.syn.aco.common.dao.Dao;
import com.syn.edge.model.DataTag;

public interface DataTagDao extends Dao<DataTag>{
	
}
