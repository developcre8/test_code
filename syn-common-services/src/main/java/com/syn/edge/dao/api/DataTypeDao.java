package com.syn.edge.dao.api;


import com.syn.aco.common.dao.DaoI;
import com.syn.edge.model.DataType;

public interface DataTypeDao extends DaoI<DataType>{
	
	public int count(DataType criteria);

}
