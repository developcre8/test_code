package com.syn.edge.dao.api;

import com.syn.aco.common.dao.DaoI;
import com.syn.edge.model.Schedules;

public interface SchedulesDao extends DaoI<Schedules> {

}
