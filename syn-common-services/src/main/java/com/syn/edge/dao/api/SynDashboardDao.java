package com.syn.edge.dao.api;

import java.util.List;
import java.util.Set;

import com.syn.aco.common.dao.Dao;
import com.syn.edge.model.SynDashboard;


public interface SynDashboardDao extends Dao<SynDashboard> {
	public Set<SynDashboard> getDashboradData();
	
}
