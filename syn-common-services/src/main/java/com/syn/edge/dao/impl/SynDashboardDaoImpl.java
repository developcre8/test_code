package com.syn.edge.dao.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.syn.aco.common.dao.HibernateBaseDao;
import com.syn.edge.dao.api.SynDashboardDao;
import com.syn.edge.model.SynDashboard;



@Repository("synDashboardDao")
public class SynDashboardDaoImpl implements SynDashboardDao {

	@Autowired
	private HibernateBaseDao hibernateBaseDao;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public SynDashboard findById(Serializable id) {
		
		return (SynDashboard)hibernateBaseDao.getObjectById("synDashboard", id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SynDashboard> findAll() {
		/*Map where = new HashMap();
		List<SynDashboard> dashBoardList = (List<SynDashboard>)hibernateBaseDao.getAllListWithWhere("synDashboard", where);*/
		Session session = sessionFactory.openSession();
		Criteria query=session.createCriteria("synDashboard");
		//query.setCacheable(true);
		query.setFetchMode("widgets", FetchMode.JOIN);
		query.setFetchMode("synDashboardWidgetsValue1s", FetchMode.JOIN);
		query.setFetchMode("synDashboardWidgetsValue2s", FetchMode.JOIN);
		List<SynDashboard> dashBoardList = query.list();
		
		return dashBoardList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SynDashboard> find(SynDashboard searchCriteria) {
		Map where = new HashMap();
		return (List<SynDashboard>)hibernateBaseDao.getAllListWithWhere("synDashboard", where);
	}

	@Override
	public boolean isDuplicate(SynDashboard entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public SynDashboard insert(SynDashboard entity) {
		Long id = (Long)hibernateBaseDao.saveData("synDashboard", entity);
		return findById(id);
	}

	@Override
	public SynDashboard update(SynDashboard entity) {
		Boolean isUpdate = hibernateBaseDao.updateData("synDashboard", entity);		
		return findById(entity.getId());
	}

	@Override
	public void delete(Long id) {
		
		
	}

	@Override
	public int count() {
		
		return 0;
	}

	@Override
	public List<SynDashboard> findByLimit(SynDashboard criteria, int start,
			int limit) {
		
		return null;
	}

	@Override
	public List<SynDashboard> getByLimit(int start, int limit) {
		
		return null;
	}
	
	public Set<SynDashboard> getDashboradData()	{
		//List<SynDashboard> list=new ArrayList<SynDashboard>();
		String Query= ""
				+ "SELECT SD.dashboardid, "
				+ "       SD.link, "
				+ "       SD.container, "
				+ "       SDW.widgetid, "
				+ "       SDW.row, "
				+ "       SDW.seq, "
				+ "       SDW.widget, "
				+ "       SDW.name, "
				+ "       SDW.KEYS, "
				+ "       SDW.drilldown, "
				+ "       SDW.filter, "
				+ "       SDW.create_user_id, "
				+ "       SDW.create_datetime, "
				+ "       SDW.update_user_id, "
				+ "       SDW.update_datetime, "
				+ "       SDWV1.widgetid, "
				+ "       SDWV1.widgetvalueid, "
				+ "       SDWV1.vendorname, "
				+ "       SDWV1.recordcount, "
				+ "       SDWV1.codetype, "
				+ "       SDWV1.filecount, "
				+ "       SDWV1.month, "
				+ "       SDWV2.widgetvalueid, "
				+ "       SDWV2.widgetid , "
				+ "       SDWV2.count, "
				+ "       SDWV2.vendor "
				+ "FROM   syn_dashboard SD "
				+ "        LEFT OUTER JOIN syn_dashboard_widgets SDW "
				+ "               ON SD.dashboardid = SDW.dashboardid "
				+ "        LEFT OUTER JOIN syn_dashboard_widgets_value1 SDWV1 "
				+ "               ON SDW.widgetid = SDWV1.widgetid "
				+ "       LEFT OUTER JOIN syn_dashboard_widgets_value2 SDWV2 "
				+ "                    ON SDW.widgetid = SDWV2.widgetid";
		Session session=null;
		Set<SynDashboard> dashboardSet = new HashSet<SynDashboard>();
		try{
		session=sessionFactory.openSession();
		SQLQuery query=session.createSQLQuery(Query);
		query.addEntity("synDashboard","synDashboard_query");
		query.addJoin("synDashboardWidgets","synDashboard.widgets_set");
		query.addJoin("synDashboardWidgetsValue1s_set","synDashboardWidgets.synDashboardWidgetsValue1s_set");
		query.addJoin("synDashboardWidgetsValue2s_set", "synDashboardWidgets.synDashboardWidgetsValue2s_set");
		/*query.addEntity("synDashboard","synDashboard");
		query.addEntity("synDashboardWidgets","synDashboardWidgets");
		query.addEntity("synDashboardWidgetsValue1s_set","synDashboardWidgetsValue1");
		query.addEntity("synDashboardWidgetsValue2s_set", "synDashboardWidgetsValue2");*/
		List<SynDashboard> list=query.list();	
		
		
		Object[] objects = null;
		
		for(Iterator it = list.iterator(); it.hasNext();){
		
			objects = (Object[])it.next();
			SynDashboard dashboard =(SynDashboard)objects[0];
			dashboardSet.add(dashboard);
		
		}
		}catch(HibernateException hbexp)
		{
			System.out.println(hbexp);
		}
		
		return dashboardSet;		
		
	}
	

}
