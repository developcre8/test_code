package com.syn.edge.services.api;

import java.util.List;

import com.syn.aco.common.services.BaseServices;
import com.syn.edge.model.SynDashboard;



public interface SynDashboardService extends BaseServices<SynDashboard, Long> {
	List<SynDashboard> getDashboradData();
}
