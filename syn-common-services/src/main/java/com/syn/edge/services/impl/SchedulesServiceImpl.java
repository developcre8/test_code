package com.syn.edge.services.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syn.aco.common.exception.EntityNotFoundException;
import com.syn.aco.security.utils.CurrentUser; 
import com.syn.edge.dao.api.SchedulesDao;
import com.syn.edge.model.Schedules;
import com.syn.edge.services.api.SchedulesService;

@Service("schedulesService")
public class SchedulesServiceImpl implements SchedulesService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private SchedulesDao schedulesDao;

	@Override
	public Schedules get(String id) {
		Schedules data = null;
		if(id != null){
			data=schedulesDao.findById(id);
		}
		logger.debug("Schedules => " + data);
	    if (data == null)
	    {
	    	 throw new EntityNotFoundException("Schedules not found with id : " + id);
	    }
		return data;
	}

	@Override
	public Schedules create(Schedules resource) {
		resource.setCreateUserId(CurrentUser.getCurrentUser().intValue());
		resource.setCreateDateTime(new Date());
		resource.setUpdateUserId(CurrentUser.getCurrentUser().intValue());
		resource.setUpdateDateTime(new Date());
		resource = schedulesDao.insert(resource);
		return resource;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Schedules update(Schedules resource, String id) {
		resource.setId(id);
		resource.setCreateUserId(CurrentUser.getCurrentUser().intValue());
		resource.setCreateDateTime(new Date());
		resource.setUpdateUserId(CurrentUser.getCurrentUser().intValue());
		resource.setUpdateDateTime(new Date());
		return schedulesDao.update(resource);
	}

	@Override
	public List<Schedules> findAll() {
		List<Schedules> allList =  schedulesDao.findAll();
		logger.debug("SchedulesList => " + allList);
	    if (allList == null)
	    {
	      throw new EntityNotFoundException("Schedules List not found ");
	    }
		return allList;
	}

	@Override
	public List<Schedules> find(Schedules criteria) {
		return schedulesDao.find(criteria);
	}

	@Override
	public List<Schedules> findByLimit(Schedules criteria, int start, int limit) {
		List<Schedules> allList = schedulesDao.findByLimit(criteria, start, limit);
		logger.debug("SchedulesList => " + allList);
	    if (allList == null)
	    {
	      throw new EntityNotFoundException("Schedules List not found ");
	    }
		return allList;
	}

	@Override
	public List<Schedules> getByLimit(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isDuplicateCreate(Schedules entity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDuplicateUpdate(Schedules entity, String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int count(Schedules criteria) {
		// TODO Auto-generated method stub
		return 0;
	}

}
