package com.syn.edge.services.impl;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syn.edge.dao.api.DataTagDao;
import com.syn.edge.model.DataTag;
import com.syn.edge.services.api.DataTagService;

@Service("dataTagService")
public class DataTagServiceImpl implements DataTagService {
	
	@Autowired
	private DataTagDao dataTagDao;

	@Override
	public DataTag get(String id) {
		return dataTagDao.findById(id);
	}

	@Override
	public DataTag create(@Valid DataTag resource) {
		
		resource = dataTagDao.insert(resource);
		return resource;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DataTag update(DataTag resource, String id) {
		
		
		return dataTagDao.update(resource);
	}

	@Override
	public List<DataTag> findAll() {
		return dataTagDao.findAll();
	}

	@Override
	public List<DataTag> find(DataTag criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DataTag> findByLimit(DataTag criteria,
			int start, int limit) {
		return null;
	}

	@Override
	public List<DataTag> getByLimit(int start, int limit) {
		return dataTagDao.getByLimit(start, limit);
	}
	
	@Override
	public Integer count() {
		return dataTagDao.count();
	}


}