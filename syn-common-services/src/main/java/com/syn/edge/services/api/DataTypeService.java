package com.syn.edge.services.api;

import com.syn.aco.common.services.BaseServicesI;
import com.syn.edge.model.DataType;

public interface DataTypeService extends BaseServicesI<DataType, String> {
	public Integer count();
	public int count(DataType criteria);
	// public boolean isDuplicate(DataType entity);

}



