package com.syn.edge.services.api;

import java.util.List;

import com.syn.aco.common.services.BaseServicesI;
import com.syn.edge.model.EdgeCode;

public interface EdgeCodesService extends BaseServicesI<EdgeCode, String> {

	public List<EdgeCode> getByCode(String code);

	public List<EdgeCode> getByType(String type);

	public List<EdgeCode> likeByName(String name);
	
	public List<EdgeCode> getEdgeCode(EdgeCode edgeCode);
}
