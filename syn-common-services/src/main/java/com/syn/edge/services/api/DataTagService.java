package com.syn.edge.services.api;


import java.util.List;

import com.syn.aco.common.services.BaseServices;
import com.syn.edge.model.DataTag;

public interface DataTagService extends BaseServices<DataTag, String>{
	public Integer count();
}
