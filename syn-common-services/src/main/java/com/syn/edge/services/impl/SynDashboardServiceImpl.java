package com.syn.edge.services.impl;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syn.edge.dao.api.SynDashboardDao;
import com.syn.edge.model.SynDashboard;
import com.syn.edge.model.SynDashboardWidgets;
import com.syn.edge.model.SynDashboardWidgetsValue1;
import com.syn.edge.model.SynDashboardWidgetsValue2;
import com.syn.edge.services.api.SynDashboardService;

@Service("synDashboardService")
public class SynDashboardServiceImpl implements SynDashboardService {

	@Autowired
	private SynDashboardDao synDashboardDao;
	
	@Override
	public SynDashboard get(Long id) {
		// TODO Auto-generated method stub
		return synDashboardDao.findById(id);
	}

	@Override
	public SynDashboard create(@Valid SynDashboard resource) {
		
		
		resource = synDashboardDao.insert(resource);
		return resource;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public SynDashboard update(SynDashboard resource, Long id) {
		resource.setId(Long.valueOf(id));
		
		return synDashboardDao.update(resource);
	}

	@Override
	public List<SynDashboard> findAll() {

		List<SynDashboard> list=synDashboardDao.findAll();
		
		for(int i=0;i<list.size();i++)
		{
			SynDashboard syndashboard=(SynDashboard)list.get(i);	
			List<SynDashboardWidgets> syndashboardwidgets=syndashboard.getWidgets();
		      for(int j=0;j<syndashboardwidgets.size();j++)
		      {
		    	
		    	  SynDashboardWidgets syndashboardwidget=(SynDashboardWidgets)syndashboardwidgets.get(j); 
		    	  List keylist=new ArrayList();
		    	  String keys=syndashboardwidget.getKey();
		    	  String str[]=keys.split(",");
		    	  keylist=Arrays.asList(str);
		    	  syndashboardwidget.setKeys(keylist);
		    	  List namelist=new ArrayList();
		    	  String names=syndashboardwidget.getNames();
		    	  String str1[]=names.split(",");
		    	  namelist=Arrays.asList(str1);
		    	  syndashboardwidget.setName(namelist);
		    	  List<SynDashboardWidgetsValue1> SynDashboardWidgetsValue1list=syndashboardwidget.getSynDashboardWidgetsValue1s();
		    	  List<SynDashboardWidgetsValue2> SynDashboardWidgetsValue2list=syndashboardwidget.getSynDashboardWidgetsValue2s();		    	  
		          if(!SynDashboardWidgetsValue1list.isEmpty())
		          {
		        	  syndashboardwidget.setValues(SynDashboardWidgetsValue1list);  
		          }
		          if(!SynDashboardWidgetsValue2list.isEmpty())
		          {
		        	  syndashboardwidget.setValues(SynDashboardWidgetsValue2list);  
		          }
		      
		      }
		  	   
		      syndashboard.setWidgets(syndashboardwidgets);
		     
		}
		 System.out.println("size"+list.size());
		return list;
	}

	
	
	public List<SynDashboard> getDashboradData() {
		List<SynDashboard> dashBoardList = new ArrayList<SynDashboard>();
		Set<SynDashboard> dashboards=synDashboardDao.getDashboradData();
		SynDashboard syndashboard = null;
		for(SynDashboard syndashboardValue : dashboards){
			syndashboard = new SynDashboard();
			List<SynDashboardWidgets> dashboardwidgetList=new ArrayList<SynDashboardWidgets>();
			
			Set<SynDashboardWidgets> syndashboardwidgetsSet=syndashboardValue.getWidgets_set();
			SynDashboardWidgets syndashboardwidget = null;
		      for(SynDashboardWidgets syndashboardwidgetValue : syndashboardwidgetsSet){
		    	  syndashboardwidget = new SynDashboardWidgets();
		    	  //SynDashboardWidgets syndashboardwidget=(SynDashboardWidgets)syndashboardwidgets.get(j); 
		    	  List keylist=new ArrayList();
		    	  String keys=syndashboardwidgetValue.getKey();
		    	  String str[]=keys.split(",");
		    	  keylist=Arrays.asList(str);
		    	  syndashboardwidget.setKeys(keylist);
		    	  List namelist=new ArrayList();
		    	  String names=syndashboardwidgetValue.getNames();
		    	  String str1[]=names.split(",");
		    	  namelist=Arrays.asList(str1);
		    	  syndashboardwidget.setName(namelist);
		    	  /*Set<SynDashboardWidgetsValue1> dashboardWidgetsValue1Set=;
		    	  Set<SynDashboardWidgetsValue2> dashboardWidgetsValue2Set=syndashboardwidget.getSynDashboardWidgetsValue2s_set();*/
		    	
		    	  Iterator<SynDashboardWidgetsValue1> dashBoardValue1=syndashboardwidgetValue.getSynDashboardWidgetsValue1s_set().iterator();
		    	  Iterator<SynDashboardWidgetsValue2> dashBoardValue2=syndashboardwidgetValue.getSynDashboardWidgetsValue2s_set().iterator();
		    	  List<SynDashboardWidgetsValue1> dashboardWidgetsValue1list=null;
		  		 List<SynDashboardWidgetsValue2> dashboardWidgetsValue2list=null;
		    	  if(!syndashboardwidgetValue.getSynDashboardWidgetsValue1s_set().isEmpty()){
		    		  dashboardWidgetsValue1list=new ArrayList<SynDashboardWidgetsValue1>();
		    		  while(dashBoardValue1.hasNext()){
		    			  SynDashboardWidgetsValue1 dashboardWidgetsValue1 = dashBoardValue1.next();
		    			  dashboardWidgetsValue1list.add(dashboardWidgetsValue1);
		    		  }
		    		 syndashboardwidget.setValues(dashboardWidgetsValue1list);
		    		
		    		 //syndashboardwidget.getValues().add(syndashboardwidget);
		          }
		          if(!syndashboardwidgetValue.getSynDashboardWidgetsValue2s_set().isEmpty()){
		        	  dashboardWidgetsValue2list=new ArrayList<SynDashboardWidgetsValue2>();
		        	  while(dashBoardValue2.hasNext()){
		        		  SynDashboardWidgetsValue2 dashboardWidgetsValue2 = dashBoardValue2.next();
		        		  dashboardWidgetsValue2list.add(dashboardWidgetsValue2);
		    		  }
		        	  syndashboardwidget.setValues(dashboardWidgetsValue2list);
		        	  //dashboardwidgetList.add(syndashboardwidget);
		          }
		         
		         
		          syndashboard.setWidgets(dashboardwidgetList);
		      }
		      dashBoardList.add(syndashboard);
		      
		     
		}
		
		 System.out.println("size"+dashBoardList.size());
		return dashBoardList;
	}
	
	@Override
	public List<SynDashboard> find(SynDashboard criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SynDashboard> findByLimit(SynDashboard criteria, int start,
			int limit) {
		
		
		
		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SynDashboard> getByLimit(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
