package com.syn.edge.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.syn.aco.security.utils.CurrentUser;
import com.syn.edge.dao.api.EdgeCodesDao;
import com.syn.edge.model.EdgeCode;
import com.syn.edge.model.EdgeCodeId;
import com.syn.edge.services.api.EdgeCodesService;

@Service("edgeCodesService")
public class EdgeCodesServiceImpl implements EdgeCodesService {

	@Autowired
	private EdgeCodesDao edgeCodesDao;
	
	@Override
	public EdgeCode get(String id) {
		
		return edgeCodesDao.findById(id);
	}

	@Override
	public EdgeCode create(EdgeCode resource) {
		resource.setCreatedBy(CurrentUser.getCurrentUser().intValue());
		resource.setLastUpdatedBy(CurrentUser.getCurrentUser().intValue());
		resource.setCreatedDate(new Date());
		resource.setLastUpdatedDate(new Date());
		
		resource = edgeCodesDao.insert(resource);
		return resource;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public EdgeCode update(EdgeCode resource, String id) {
	/*	EdgeCodeId id = new EdgeCodeId(resource.getCode(), resource.getType());
		resource.setEdgeCodeId(id);
		resource.setLastUpdatedBy(CurrentUser.getCurrentUser().intValue());
		resource.setLastUpdatedDate(new Date());*/
		return edgeCodesDao.update(resource);
	}
	
	public EdgeCode updateByCompositeKey(EdgeCode resource) {
		EdgeCodeId id = new EdgeCodeId(resource.getCode(), resource.getType());
		resource.setId(id);
		resource.setLastUpdatedBy(CurrentUser.getCurrentUser().intValue());
		resource.setLastUpdatedDate(new Date());
		return edgeCodesDao.update(resource);
	}

	@Override
	public List<EdgeCode> findAll() {
		
		return edgeCodesDao.findAll();
	}

	@Override
	public List<EdgeCode> find(EdgeCode criteria) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EdgeCode> findByLimit(EdgeCode criteria, int start,	int limit) {
		return edgeCodesDao.findByLimit(criteria,start,limit);
	}

	@Override
	public List<EdgeCode> getByLimit(int start, int limit) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	@Transactional(readOnly = true)
	public List<EdgeCode> getEdgeCode(EdgeCode edgeCode) {
		return edgeCodesDao.getEdgeCode(edgeCode);
	}

	@Override
	public boolean isDuplicateCreate(EdgeCode entity) {
		return edgeCodesDao.isDuplicateInsert(entity);
	}

	@Override
	public boolean isDuplicateUpdate(EdgeCode entity, String id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	@Transactional(readOnly = true)
	public int count(EdgeCode criteria) {
		return edgeCodesDao.count(criteria);
	}

	@Override
	@Transactional(readOnly = true)
	public List<EdgeCode> getByCode(String code) {
		return edgeCodesDao.getByCode(code);
	}

	@Override
	@Transactional(readOnly = true)
	public List<EdgeCode> getByType(String type) {
		return edgeCodesDao.getByType(type);
	}

	@Override
	@Transactional(readOnly = true)
	public List<EdgeCode> likeByName(String name) {
		return edgeCodesDao.likeByName(name);
	}

	

}
