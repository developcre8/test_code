package com.syn.edge.services.impl;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.syn.aco.common.exception.EntityNotFoundException;
import com.syn.aco.security.utils.CurrentUser;
import com.syn.edge.dao.api.DataTagDao;
import com.syn.edge.dao.api.DataTypeDao;
import com.syn.edge.model.DataType;
import com.syn.edge.services.api.DataTypeService;

@Service("dataTypeService")
public class DataTypeServiceImpl implements DataTypeService {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired
	private DataTypeDao dataTypeDao;
	
	@Autowired
	private DataTagDao dataTagDao;

	@Override
	public DataType get(String id) {
		DataType data = null;
		if(id != null){
			data=dataTypeDao.findById(id);
		}
		logger.debug("DataType => " + data);
	    if (data == null)
	    {
	    	 throw new EntityNotFoundException("DataType not found with id : " + id);
	    }
		return data;
	}
	

	@Override
	public DataType create(@Valid DataType resource) {
		
		 resource.setCreateUserId((long) CurrentUser.getCurrentUser().intValue());
		 resource.setModifiedUserId((long) CurrentUser.getCurrentUser().intValue());
		 DataType resourceMaster = dataTypeDao.insert(resource);
		
		return resourceMaster;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DataType update(DataType resource, String id) {
		
		DataType resourceMaster = dataTypeDao.update(resource);
		 
	 return resourceMaster;
	}

	@Override
	public List<DataType> findAll() {
		return dataTypeDao.findAll();
	}

	@Override
	public List<DataType> find(DataType criteria) {
		// TODO Auto-generated method stub
		return dataTypeDao.findAll();
	}

	@Override
	public List<DataType> findByLimit(DataType criteria,
			int start, int limit) {
				
		return null;
	}

	@Override
	public List<DataType> getByLimit(int start, int limit) {
		return null;
	}
	
	@Override
	public Integer count() {
		return dataTypeDao.count();
	}

	@Override
	public boolean isDuplicateCreate(DataType entity) {
		return dataTypeDao.isDuplicateInsert(entity);
	}

	@Override
	public boolean isDuplicateUpdate(DataType entity, String id) {
		return dataTypeDao.isDuplicateUpdate(entity);
	}

	@Override
	public int count(DataType criteria) {
		// TODO Auto-generated method stub
		return dataTypeDao.count();
	}

	/*@Override
	public boolean isDuplicate(DataType entity) {
		// TODO Auto-generated method stub
		
	}*/

}